import Vue from "vue";
import Vuex from "vuex";
import Axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    page: 100,
    verzoek: {
      aggregateId: "00000000-0000-0000-0000-000000000000",
      heeftNhg: false,
      leningDelen: [],
      marktwaardeWoning: 0
    },
    response: null,
    aantalLeningdelen: 0,
    leningDeel1: null
  },
  mutations: {
    SET_PAGE(state, value) {
      state.page = value;
    },
    SET_NHG(state, value) {
      state.verzoek.heeftNhg = value;
    },
    SET_LENINGDELEN(state) {
      state.aantalLeningdelen++;
    },
    SET_LENINGDEEL(state, value) {
      state.leningDeel1 = value;
    },
    ADD_LENINGDEEL(state, value) {
      state.verzoek.leningDelen.push(value);
    },
    SET_RESPONSE(state, value) {
      state.response = value;
    },
    SET_WORTH(state, value) {
      state.verzoek.marktwaardeWoning = value;
    }
  },
  actions: {
    VolgendePagina(context, value) {
      context.commit("SET_PAGE", value);
    },
    SetNHGval(context, value) {
      context.commit("SET_NHG", value);
    },
    SetAantalLeningdelen(context) {
      context.commit("SET_LENINGDELEN");
    },
    SetLeningdeel1(context, value) {
      context.commit("SET_LENINGDEEL", value);
    },
    AddLeningDeel(context, value) {
      context.commit("ADD_LENINGDEEL", value);
    },
    SetWorth(context, value) {
      context.commit("SET_WORTH", value);
    },
    SendRequest(context) {
      Axios.post(
        "https://www.abnamro.nl/nl/mortgages/unauthenticated/oversluiten/api/oversluiten/bereken2",
        context.state.verzoek
      )
        .then(response => {
          context.commit("SET_RESPONSE", response.data);
        })
        .catch(error => {
          console.log("Error: ", error);
        });
    }
  },
  getters: {
    returnPageValue: state => {
      return state.page;
    },
    returnAantalLeningdelen: state => {
      return state.aantalLeningdelen;
    },
    returnLeningDeel1: state => {
      return state.leningDeel1;
    },
    returnLeningdelen: state => {
      return state.verzoek.leningDelen;
    },
    returnResponse: state => {
      return state.response;
    },
    returnverzoek: state => {
      return state.verzoek;
    }
  },
  modules: {}
});
