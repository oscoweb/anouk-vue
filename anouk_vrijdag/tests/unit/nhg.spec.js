import {
    shallowMount,
    createLocalVue
} from '@vue/test-utils';
import Nhg from '@/components/Nhg.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('Nhg.vue', () => {

    let dispatch;
    let store;
    let actions;

    beforeEach(()=>{
        dispatch = jest.fn();
        actions = {
            SetNHGval: jest.fn(),
            VolgendePagina: jest.fn()
        };
        store = new Vuex.Store({
            dispatch,
            actions
        });
      
    });
    it('renders props.msg when passed', () => {
        const wrapper = shallowMount(Nhg, {
            store,
            localVue
        });
        const knop = wrapper.find("#ja");
        // const NHGchoice = jest.fn();
        knop.trigger("click");
        expect(actions.SetNHGval).toHaveBeenCalled();
    });
    it('test nee', () => {
        const wrapper = shallowMount(Nhg, {
            store,
            localVue
        });
        const knop = wrapper.find("#nee");
        // const NHGchoice = jest.fn();
        knop.trigger("click");
        expect(actions.VolgendePagina).toHaveBeenCalled();
    });
});