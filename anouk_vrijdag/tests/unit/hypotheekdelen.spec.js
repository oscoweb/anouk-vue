import { shallowMount } from '@vue/test-utils'
import hypotheekdelen from '@/components/hypotheekdelen.vue'

let wrapper;
let vm;

describe('hypotheekdelen.vue', () => {

    beforeEach(() => {
        wrapper = shallowMount(hypotheekdelen, {});  // dit is je "mini browser" waar je Vue Modal (component) in getest wordt
        vm = wrapper.vm; // Dit is je Vue Modal (component) waar je dus je data, methods en html kunt vinden en testen
    });

    it('restbedrag', () => {
        // vm.restBedrag = 5555;
        expect(vm.restBedrag).toEqual(0);
    })
})