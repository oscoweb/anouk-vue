import {
    shallowMount,
    createLocalVue
} from '@vue/test-utils';
import Vuex from 'vuex';
import mogelijkheden from '@/components/mogelijkheden.vue';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('mogelijkheden.vue', () => {

    let getters;
    let store;


    beforeEach(()=>{
        getters = {
            returnPageValue: jest.fn()
        };
        store = new Vuex.Store({
            getters
        });
    });

    it('renders props.msg when passed', () => {
        const wrapper = shallowMount(mogelijkheden, {
            store,
            localVue
        });
        expect(wrapper).toBeDefined();
    });
});